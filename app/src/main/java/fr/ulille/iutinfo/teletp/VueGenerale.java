package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;
    private String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel suiviViewModel;
    private Spinner spSalle;
    private Spinner spPoste;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        poste="";
        salle=DISTANCIEL;

        // TODO Q2.c
        suiviViewModel = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        // TODO Q4
        spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapterSalle = ArrayAdapter.createFromResource(super.getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        adapterSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adapterSalle);
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salle = parent.getItemAtPosition(position)+"";
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(super.getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(adapter);
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView login = (TextView) getActivity().findViewById(R.id.tvLogin);
            suiviViewModel.setUsername(login.getText()+"");
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        update();
        // TODO Q9
    }

    // TODO Q5.a
    private void update() {
        if (salle.equals(DISTANCIEL)){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
        }
        suiviViewModel.setLocalisation(salle);
    }

        // TODO Q9
}