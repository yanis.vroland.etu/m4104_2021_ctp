package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder> /* TODO Q6.a */ {

    private SuiviViewModel suiviViewModel;
    private LayoutInflater mInflater;

    // TODO Q6.a

    public SuiviAdapter(Context context, SuiviViewModel suiviViewModel){
        this.suiviViewModel = suiviViewModel;
        this.mInflater = LayoutInflater.from(context);
    }

    // construit les lignes à partir du xml quand c'est necessaire
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    // relie les données au textView dans chaque ligne
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String OS= suiviViewModel.getQuestions(position);
        holder.myTextView.setText(OS);
    }
    // nombre total de ligne
    @Override
    public int getItemCount() {return suiviViewModel.getQuestions().length;}
    // stocke et reafiche les views au fur et à mesure du scrolling sur l'écran

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.question);
            //itemView.setOnClickListener(this);
        }
    }
    // TODO Q7


}
